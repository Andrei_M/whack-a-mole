#include "randomutil.h"

namespace RandomUtil {


std::default_random_engine& globalEngine()
{
    static std::default_random_engine engine{};
    return engine;
}

void randomize()
{
    static std::random_device device{};
    globalEngine().seed(device());
}

int nextInt(int low, int high)
{
    std::uniform_int_distribution<> dist{low, high};
    return dist(globalEngine());
}

}
