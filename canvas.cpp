#include "canvas.h"

#include <iostream>
#include <assert.h>
#include <QPainter>
#include <QMouseEvent>
#include <QTimer>

#include "randomutil.h"

Canvas::Canvas(QWidget *parent) : QFrame(parent)
{
    mTarget.setShape(TargetShape::Circle);
    mTarget.setPosition(TargetPosition{0, 0});

    mTargetMoverTimer = new QTimer(this);
    connect(mTargetMoverTimer,
            &QTimer::timeout,
            this,
            &Canvas::onTargetLost);
    mTargetMoverTimer->start(1000); // trigger the timer once every second
}

QSize Canvas::minimumSizeHint() const
{
    return QSize{100, 100};
}

void Canvas::resizeEvent(QResizeEvent *)
{
    updateTargetRect();
}

/*!
 * \brief Canvas::generateRandomTargetPosition
 */
void Canvas::generateRandomTargetPosition()
{
    TargetPosition targetPosition;
    targetPosition.row = RandomUtil::nextInt(0, mNbRows-1);
    targetPosition.column = RandomUtil::nextInt(0, mNbColumns-1);
    mTarget.setPosition(targetPosition);
}

void Canvas::onTargetLost()
{
    std::cout << "lost" << std::endl;
    generateRandomTargetPosition();
    updateTargetRect();
    update();
    mMissedCounter++;
    emit targetMissed(mMissedCounter);
}

void Canvas::mousePressEvent(QMouseEvent *event)
{
    auto pressPos = event->pos();
    if (getTargetShapeRect().contains(pressPos))
    {
        mTargetMoverTimer->stop();
        generateRandomTargetPosition();
        updateTargetRect();
        update();
        mHitCounter++;
        emit targetHit(mHitCounter);
        mTargetMoverTimer->start(1000);
    }
    else
    {
        mMissedCounter++;
        emit targetMissed(mMissedCounter);
    }
}

void Canvas::setTargetShape(TargetShape const& shape)
{
    if (mTarget.getShape() != shape) {
        mTarget.setShape(shape);
        update();
    }
}


void Canvas::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing); // draw smooth lines
    painter.fillRect(rect(), Qt::black);

    drawGrid(painter);
    drawTarget(painter);
}

void Canvas::drawGrid(QPainter & painter)
{
    if (mNbRows <= 0 || mNbColumns <= 0 || height()==0 || width()==0)
    {
        return;
    }

    painter.save();

    // Pen & Brush
    // pen -> contours
    painter.setPen(Qt::green);
    // brush -> filling
    painter.setBrush(Qt::green);

    int left = rect().left();
    int right = rect().right();
    int top = rect().top();
    int bottom = rect().bottom();

    // horizantal lines
    int rowStep = height() / mNbRows;
    for (int row=top; row<=bottom; row+=rowStep)
    {
        painter.drawLine(QPoint(left, row),
                         QPoint(right, row));
    }

    // vertical lines
    int columnStep = width() / mNbColumns;
    for (int col=left; col<=right; col+=columnStep)
    {
        painter.drawLine(QPoint(col, top),
                         QPoint(col, bottom));
    }

    painter.restore();
}

void Canvas::updateTargetRect()
{
    assert(mNbRows > 0);
    assert(mNbColumns > 0);
    if (mNbRows <= 0 || mNbColumns <=0)
    {
        mTargetRect = QRect();
        mPixmapRect = QRect();
        return;
    }

    int gridCellWidth = width() / mNbColumns;
    int gridCellHeight = height() / mNbRows;

    auto targetPos = mTarget.getPosition();
    int top = targetPos.row * gridCellHeight;
    int left = targetPos.column * gridCellWidth;
    mTargetRect.setTop(top);
    mTargetRect.setLeft(left);
    mTargetRect.setWidth(gridCellWidth);
    mTargetRect.setHeight(gridCellHeight);

    mScaledPixmap = mTargetPixmap.scaled(mTargetRect.size(), Qt::KeepAspectRatio);
    mPixmapRect = mScaledPixmap.rect();
    mPixmapRect.moveCenter(mTargetRect.center());
}

QRect Canvas::getTargetShapeRect() const
{
    if (mTarget.getShape()==TargetShape::Pixmap) {
        return mPixmapRect;
    }

    return mTargetRect;
}

void Canvas::drawTarget(QPainter& painter)
{
    painter.save();
    painter.setPen(Qt::yellow);
    painter.setBrush(Qt::yellow);

    QRect shapeRect = getTargetShapeRect();
    switch(mTarget.getShape()) {
        case TargetShape::Rectangle:
            painter.drawRect(shapeRect);
            break;

        case TargetShape::Circle:
            painter.drawEllipse(shapeRect);
            break;

        case TargetShape::Pixmap:
        {
            painter.drawPixmap(shapeRect, mScaledPixmap);
            break;
        }
        default:
            break;
    }

    painter.restore();
}

void Canvas::setNbRows(int nbRows)
{
    if (nbRows>0 && mNbRows != nbRows) {
        mNbRows = nbRows;
        updateTargetRect();
        update();
    }
}

void Canvas::setNbColumns(int nbColumns)
{
    if (nbColumns>0 && mNbColumns != nbColumns) {
        mNbColumns = nbColumns;
        updateTargetRect();
        update();
    }
}
