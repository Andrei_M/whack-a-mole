#include "target.h"

#include <QObject>

#include <assert.h>

Target::Target()
{

}

QString targetShapeString(TargetShape const& shape)
{
    switch(shape) {
    case TargetShape::Rectangle:
        return QObject::tr("Rectangle");

    case TargetShape::Circle:
        return QObject::tr("Circle");

    case TargetShape::Pixmap:
        return QObject::tr("Pixmap");

    default:
        assert(!"Shape not recognized:");
        return "";
    }
}
