#ifndef TARGET_H
#define TARGET_H

#include <QString>
#include <QMetaType>

/*!
 * \brief The TargetShape enum
 */
enum class TargetShape
{
    Rectangle,
    Circle,
    Pixmap
};

/*! Register the TargetShape type into Qt's Meta-Object System,
 *  so it can be used with QVariant.
*/
Q_DECLARE_METATYPE(TargetShape)

QString targetShapeString(TargetShape const& shape);

/*!
 * \brief The TargetPosition struct
    Simple struct to represent a position in a grid (row and column)
 */
struct TargetPosition
{
    int row;
    int column;
};

/*!
 * \brief The Target class
    This class represents the target that is drawn on the screen. As such,
    it has a position and a shape
 */
class Target
{
public:
    Target();

public:
    auto getShape() const -> TargetShape { return mShape; }
    auto getPosition() const -> TargetPosition { return mPosition; }
    void setShape(TargetShape const & shape) { mShape = shape; }
    void setPosition(TargetPosition const & pos) { mPosition = pos; }

private:
    TargetShape mShape;
    TargetPosition mPosition;
};

#endif // TARGET_H
