#include "mainwindow.h"
#include <QApplication>
#include <QTime>
#include "randomutil.h"

int main(int argc, char *argv[])
{
    RandomUtil::randomize();

    // The main application, used for handling events (through an event loop)
    QApplication a(argc, argv);

    // create the window and make it visible
    MainWindow w;
    w.show();

    // start the event loop
    return a.exec();
}
