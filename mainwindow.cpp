#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QVariant>
#include "target.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    initControls();
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::initControls()
{
    ui->mRowsSpinBox->setRange(1, 10);
    ui->mRowsSpinBox->setValue(ui->mCanvas->nbRows());

    ui->mColumnsSpinBox->setRange(1, 10);
    ui->mColumnsSpinBox->setValue(ui->mCanvas->nbColumns());


    connect(ui->mRowsSpinBox,
            static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),
            ui->mCanvas,
            &Canvas::setNbRows);

    connect(ui->mColumnsSpinBox,
            static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),
            ui->mCanvas,
            &Canvas::setNbColumns);

    QString itemText;
    QVariant itemData;

    // Rectangle item (text and TargetShape)
    itemText = targetShapeString(TargetShape::Rectangle);
    itemData =  QVariant::fromValue<TargetShape>(TargetShape::Rectangle);
    ui->mShapeComboBox->addItem(itemText, itemData);

    // Circle item (text and TargetShape)
    itemText = targetShapeString(TargetShape::Circle);
    itemData = QVariant::fromValue<TargetShape>(TargetShape::Circle);
    ui->mShapeComboBox->addItem(itemText, itemData);

    // Pixmap item (text and TargetShape)
    itemText = targetShapeString(TargetShape::Pixmap);
    itemData = QVariant::fromValue<TargetShape>(TargetShape::Pixmap);
    ui->mShapeComboBox->addItem(itemText,itemData);

    // set the value that's currently in the canvas
    TargetShape shape = ui->mCanvas->getTargetShape();
    QVariant shapeVariant = QVariant::fromValue<TargetShape>(shape);
    int shapeIndex = ui->mShapeComboBox->findData(shapeVariant);
    ui->mShapeComboBox->setCurrentIndex(shapeIndex);

    connect(ui->mShapeComboBox,
            static_cast<void (QComboBox::*)  (int)>(&QComboBox::currentIndexChanged),
            this,
            &MainWindow::onShapeIndexChanged);

    connect(ui->mCanvas,
            &Canvas::targetHit,
            this,
            &MainWindow::onTargetHit);

    connect(ui->mCanvas,
            &Canvas::targetMissed,
            this,
            &MainWindow::onTargetMissed);
}

void MainWindow::onShapeIndexChanged(int index)
{
    QVariant itemData = ui->mShapeComboBox->itemData(index);
    TargetShape shape = itemData.value<TargetShape>();
    ui->mCanvas->setTargetShape(shape);
}

void MainWindow::onTargetHit(int hitCount)
{
    QString hitText = QString("Hit: %1").arg(hitCount);
    ui->mHitLabel->setText(hitText);

    if (hitCount % 100 == 0) {
        auto statusMessage = QString("Checkpoint achieved: %1 hits").arg(hitCount);
        ui->statusBar->showMessage(statusMessage, 3000);
    }
}

void MainWindow::onTargetMissed(int missedCount)
{
    QString missedText = QString("Missed: %1").arg(missedCount);
    ui->mMissedLabel->setText(missedText);
}

