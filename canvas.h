#ifndef CANVAS_H
#define CANVAS_H

#include <memory>

//@{
/*! Qt includes and forward declarations
 */
#include <QFrame>
#include <QPixmap>
QT_BEGIN_NAMESPACE
class QTimer;
class QPixmap;
QT_END_NAMESPACE
//@}

#include "target.h"

/*!
 * \brief The widget that draws the game "scene" (the grid and the target)
 * It inherits QFrame, so that we can draw a frame around it.
 */
class Canvas : public QFrame
{
    //! Q_OBJECT macro, needed to enable signals and slots (among other things)
    Q_OBJECT
public:
    //! Set a valid parent when constructing this widget so that memory is
    //! managed automatically by Qt (the widget is deleted automatically
    //! by its parent)
    explicit Canvas(QWidget *parent = nullptr);

    //@{
    //! how the widget is drawn
    virtual void paintEvent(QPaintEvent *) override;
    //! control what happens when the widget is resized
    virtual void resizeEvent(QResizeEvent *) override;
    //! control what happens when the user clicks on the widget
    virtual void mousePressEvent(QMouseEvent *event) override;
    //! ensure the widget will not be resized smaller than we want
    virtual QSize minimumSizeHint() const override;
    //@}

    int nbRows() const { return mNbRows; }
    int nbColumns() const { return mNbColumns; }
    TargetShape getTargetShape() const { return mTarget.getShape();  }

signals:
    void targetHit(int totalHitCount) const;
    void targetMissed(int totalMissedCount) const;

public slots:
    void setNbRows(int nbRows);
    void setNbColumns(int nbColumns);
    void setTargetShape(TargetShape const&);

private slots:
    void onTargetLost();

private:
    QRect getTargetShapeRect() const;
    void drawGrid(QPainter &);
    void drawTarget(QPainter &);
    void updateTargetRect();
    void generateRandomTargetPosition();

private:
    int mNbRows = 3;
    int mNbColumns = 3;

    Target mTarget;
    QRect mTargetRect; /*!< the bounding rect of the target. The target is drawn
        within this rectangle.*/

    int mHitCounter = {0};
    int mMissedCounter = {0};

    QTimer * mTargetMoverTimer; /*!< timer that moves the target to a new
         position automatically if target is not hit in time */
    QPixmap mTargetPixmap = QPixmap(":/pixmaps/cpp"); /*!< access the resources
        by prefixing the path with the : (colon) symbol */
    QPixmap mScaledPixmap; /*!< rescaled pixmap, so it fits inside a grid space */
    QRect mPixmapRect; /*!< the bounding box of the pixmap, which is always
        smaller than the target rect. The pixmap is drawn within this rectangle */
};

#endif // CANVAS_H
