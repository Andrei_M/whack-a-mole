#ifndef RANDOMUTIL_H
#define RANDOMUTIL_H

#include <random>

namespace RandomUtil
{
    std::default_random_engine& globalEngine();
    void randomize();
    int nextInt(int low, int high);
};

#endif // RANDOMUTIL_H
