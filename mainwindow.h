#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

/*!
 *  \brief The main window of the game.
    The widgets are added using the QtDesigner, and available through the
    Ui::MainWindow * pointer.
 */
class MainWindow : public QMainWindow
{
    //! Q_OBJECT macro, needed to enable signals and slots (among other things)
    Q_OBJECT

public:
    //! Set a valid parent when constructing this widget so that memory is
    //! managed automatically by Qt (the widget is deleted automatically
    //! by its parent)
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void onShapeIndexChanged(int);
    void onTargetHit(int);
    void onTargetMissed(int);

private:
    void initControls();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
